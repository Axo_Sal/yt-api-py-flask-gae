"""`main` is the top level module for your Flask application."""
# Note: We don't need to call run() since our application is embedded within
# the App Engine WSGI application server.

import json
import flask
import httplib2
import urllib2
import uuid
from flask import render_template
from oauth2client import client
from apiclient.discovery import build

app = flask.Flask(__name__)

@app.errorhandler(404)
def page_not_found(e):
    """Return a custom 404 error."""
    return 'Sorry, Nothing at this URL.', 404

@app.errorhandler(500)
def application_error(e):
    """Return a custom 500 error."""
    return 'Sorry, unexpected error: {}'.format(e), 500

playlistId = None

@app.route('/')
def index():
    if 'credentials' not in flask.session:
        return render_template('html/preAuth.html')

    credentials = client.OAuth2Credentials.from_json(flask.session['credentials'])

    if credentials.access_token_expired:
        return render_template('html/preAuth.html')

    else:
        return render_template('html/loading.html')

# if you change the name of the route and function change it in loading.jade rediraction too
@app.route('/application')
def application():
    if 'credentials' not in flask.session:
        return render_template('html/preAuth.html')

    credentials = client.OAuth2Credentials.from_json(flask.session['credentials'])

    if credentials.access_token_expired:
        return render_template('html/preAuth.html')

    try:
        youtube = build('youtube', 'v3', http=credentials.authorize(httplib2.Http()))

        request = youtube.activities().list(
          part = 'contentDetails',
          home = True,
          maxResults = 50,
          fields = 'items/contentDetails'
        )
        response = request.execute()

        listOfIds = [];
        for i in range( 0, len( response["items"] ) ):
            contentDetails = response["items"][i]["contentDetails"]

            if "upload" in contentDetails:
                listOfIds.append(contentDetails["upload"]["videoId"])

            if "like" in contentDetails:
                listOfIds.append(contentDetails["like"]["resourceId"]["videoId"])

            if "recommendation" in contentDetails:
                listOfIds.append(contentDetails["recommendation"]["resourceId"]["videoId"])

            if "bulletin" in contentDetails:
                listOfIds.append(contentDetails["bulletin"]["resourceId"]["videoId"])

            if "playlistItem" in contentDetails:
                listOfIds.append(contentDetails["playlistItem"]["resourceId"]["videoId"])

        listOfIdsWithoutDuplicates = list( set(listOfIds) )
        playlistTitle = "youtube api with python flask sample playlist"

        def createPlaylsitAndAddVideosToIt():
            playlists_insert_response = youtube.playlists().insert(
              part="snippet,status",
              body=dict(
                snippet=dict(
                  title=playlistTitle,
                  description="read the title yaow!"
                ),
                status=dict(
                  privacyStatus="unlisted"
                )
              )
            ).execute()

            global playlistId
            playlistId = playlists_insert_response['id']

            def addToPlaylist(videoId):
                youtube.playlistItems().insert(
                    part='snippet',
                    body=dict(
                        snippet=dict(
                            playlistId=playlistId,
                            position=0,
                            resourceId=dict(
                                videoId=videoId,
                                kind='youtube#video'
                            )
                        )
                    )
                ).execute()

            for videoId in listOfIdsWithoutDuplicates:
                addToPlaylist(videoId)

        # out of the createPlaylsitAndAddVideosToIt function
        try:
            playlists = youtube.playlists().list(
                part='snippet',
                mine=True,
                maxResults=50,
                fields='items(id,snippet)'
            ).execute()
        except:
            return 'You need to create a youtube channel to be able to make playlists.&nbsp;<a target="_blank" href="https://support.google.com/youtube/answer/1646861?topic=3024170&hl=en">Create it here</a>&nbsp;and then reload this page. <br> If you see this message although you already have a youtube channel, it\'s due to network error. Reload this page to try again.'

        listOfPlaylists = []

        for i in playlists['items']:
            listOfPlaylists.append( i['snippet']['title'] )

        if playlistTitle in listOfPlaylists:
            IdOfPlaylsitToDelete = playlists['items'][ listOfPlaylists.index(playlistTitle) ]['id']
            youtube.playlists().delete(
                id=IdOfPlaylsitToDelete
            ).execute()
            createPlaylsitAndAddVideosToIt()
            return render_template('html/postAuth.html', playlistId=playlistId)
        if playlistTitle not in listOfPlaylists:
            createPlaylsitAndAddVideosToIt()
            return render_template('html/postAuth.html', playlistId=playlistId)

    except:
        return render_template('html/preAuth.html')

access_token = None

app.secret_key = str(uuid.uuid4())

@app.route('/oauth2callback')
def oauth2callback():
  flow = client.flow_from_clientsecrets(
      'client_secrets.json',
      scope='https://www.googleapis.com/auth/youtube',
      redirect_uri= flask.url_for('oauth2callback', _external=True)
      )
  if 'code' not in flask.request.args:
    auth_uri = flow.step1_get_authorize_url()
    return flask.redirect(auth_uri)
  else:
    auth_code = flask.request.args.get('code')

    credentials = flow.step2_exchange(auth_code)

    flask.session['credentials'] = credentials.to_json()

    authResponse = json.loads(flask.session['credentials'])

    global access_token
    access_token = authResponse['access_token']

    return flask.redirect(flask.url_for('index'))

@app.route('/signout')
def signout():
    urllib2.urlopen("https://accounts.google.com/o/oauth2/revoke?token=" + str(access_token) ).read()
    return render_template('html/preAuth.html')
